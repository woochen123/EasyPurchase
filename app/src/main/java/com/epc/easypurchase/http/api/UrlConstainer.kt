package com.jiazhuo.blockgamesquare.http.api

/**
 * api地址
 */
object UrlConstainer {
    var appBaseUrl = ""//app接口地址
    var webBaseUrl = ""//h5页面地址
    val APP_TEST_URL = "http://221.232.55.238:8811"//app测试地址
    val APP_RELEASE_URL = "http://221.232.55.238:8811"//app线上地址
    val WEB_TEST_URL = "http://221.232.55.238:8801"//h5测试地址
    val WEB_RELEASE_URL = "http://221.232.55.238:8801"//h5线上地址

    /**
     * 登录
     */
    const val LOGIN = "/ms/public/roleLogin"

    /**
     *注册
     */
    const val REGISTER = "/ms/public/register"

    /**
     * 忘记密码
     */
    const val FORGET_PWD = "/ms/public/modifyPassword"

    /**
     * 退出登录
     */
    const  val LOGOUT = "/ms/public/roleLoginOut"

    /**
     * 发送验证码
     */
    const val SEND_CODE = "/ms/public/retrieveVerifyCode/{cellphone}"

    /**
     * 完善采购人资料
     */
    const val COMPLETE_PURCHASE_DATA = "/ms/purchaser/public/clientupdatePurchaserDetail"

    /**
     * 完善供应商资料
     */
    const val COMPLETE_APPLY_DATA = "/ms/supplier/public/insertCompleteSupplierInfo"

    /**
     * 获取七牛token
     */
    const val GET_UPLOAD_TOKEN = "/ms/OssTokenController/public/getUpToken"


    /** ----------h5 start------------- **/
    /**
     * 注册协议
     */
    val REGISTER_PROTOCOL = "/registrationAgreement"

    /**
     * 首页
     */
    val HOME_PAGE = "/supplierHome"

    /**
     * 供应商消息
     */
    val PRODUCER_MSG_PAGE = "/news"

    /**
     * 供应商个人中心
     */
    val PRODUCER_MY_PAGE = "/bid"


    /**
     * 采购商消息
     */
    val CUSTOMER_MSG_PAGE = "/procureNews"

    /**
     * 采购商个人中心
     */
    val CUSTOMER_MY_PAGE = "/procurementUser"


    /** ----------h5 end------------- **/

}
