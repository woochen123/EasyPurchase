package com.jiazhuo.blockgamesquare.http.api


import com.epc.easypurchase.bean.UploadResponseBean
import com.epc.easypurchase.bean.UserBean
import com.jiazhuo.blockgamesquare.bean.BaseBean
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.*


/**
 * Created by admin on 2018/5/15.
 */

interface ApiService {


    @POST(UrlConstainer.LOGIN)
    fun login(@Body requestBody: RequestBody): Observable<BaseBean<UserBean>>


    @POST(UrlConstainer.REGISTER)
    fun register(@Body requestBody: RequestBody): Observable<BaseBean<Any>>


    @POST(UrlConstainer.FORGET_PWD)
    fun forgetPwd(@Body requestBody: RequestBody): Observable<BaseBean<Any>>

    /**
     * 完善采购人信息
     */
    @POST(UrlConstainer.COMPLETE_PURCHASE_DATA)
    fun completePurchaseData(@Body requestBody: RequestBody): Observable<BaseBean<Any>>

    /**
     * 完善供应商信息
     */
    @POST(UrlConstainer.COMPLETE_APPLY_DATA)
    fun completeSupplyData(@Body requestBody: RequestBody): Observable<BaseBean<Any>>

    /**
     * 获取上传凭证
     */
    @POST(UrlConstainer.GET_UPLOAD_TOKEN)
    fun getUploadToken(@Body requestBody: RequestBody): Observable<BaseBean<UploadResponseBean>>

    @POST(UrlConstainer.LOGOUT)
    fun logout(@Header("epc-token") token:String): Observable<BaseBean<Any>>

    @GET(UrlConstainer.SEND_CODE)
    fun sendCode(@Path("cellphone") phone:String):Observable<BaseBean<Any>>

}
