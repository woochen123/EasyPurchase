package com.jiazhuo.blockgamesquare.http.callback

/**
 * Created by chenwuchao on 2018/9/12.
 */

class ApiException(val code: Int,val msg: String) : RuntimeException(msg) {

}
