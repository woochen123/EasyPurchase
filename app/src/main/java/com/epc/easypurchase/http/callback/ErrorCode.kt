package com.jiazhuo.blockgamesquare.http.callback

/**
 * Created by Administrator on 2018/8/22.
 */

object ErrorCode {
    //customer code
    val UNKNOW = 10000

    //server code
    val UNKNOWN_HOST = 10001//unknown host
    val TOKEN_INVALIDE = 1002//token error


}
