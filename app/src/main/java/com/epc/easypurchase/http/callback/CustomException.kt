package com.jiazhuo.blockgamesquare.http.callback

import java.net.UnknownHostException

/**
 * Created by Administrator on 2018/8/22.
 */

class CustomException : RuntimeException {
    var code: Int = 0
    var msg: String? = null

    constructor(code: Int, message: String?) : super(message) {
        this.code = code
        this.msg = message
    }

    constructor(throwable: Throwable) : super(throwable) {
        var code = ErrorCode.UNKNOW
        var message = throwable.message
        if (throwable is ApiException) {
            code = throwable.code
            message = throwable.message
        } else if (throwable is UnknownHostException) {
            code = ErrorCode.UNKNOWN_HOST
        }
        this.code = code
        this.msg = message
    }

    override fun toString(): String {
        return "code=" + code +
                ", msg='" + msg + '\''
    }
}

