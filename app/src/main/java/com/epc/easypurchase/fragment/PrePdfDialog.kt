package com.epc.easypurchase.fragment

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.epc.easypurchase.R
import com.epc.easypurchase.AppApplication.Companion.mContext
import es.voghdev.pdfviewpager.library.RemotePDFViewPager
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter
import es.voghdev.pdfviewpager.library.remote.DownloadFile
import kotlinx.android.synthetic.main.dialog_preview_pdf.*

/**
 * Created by Administrator on 2018/9/5.
 */

open class PrePdfDialog : DialogFragment(), DownloadFile.Listener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.dialog_fragment)
    }


    var onItemClick: OnItemClick? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(R.layout.dialog_preview_pdf, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val pdfUrl = arguments.getString(PDF_URL)
        remotePDFViewPager = RemotePDFViewPager(mContext, pdfUrl, this)
        tv_close.setOnClickListener { dismiss() }
    }


    companion object {

        private val PDF_URL: String? = "pdf_url"

        fun newInsatance(pdfurl: String): PrePdfDialog {
            val prePdfDialog = PrePdfDialog()
            val budle = Bundle()
            budle.putString(PDF_URL, pdfurl)
            prePdfDialog.arguments = budle
            return prePdfDialog
        }
    }

    interface OnItemClick {
        fun click(content: String)
    }

    /* pdf--start*/
    private lateinit var adapter: PDFPagerAdapter
    private lateinit var remotePDFViewPager: RemotePDFViewPager

    override fun onFailure(e: Exception?) {
        println("pdf->" + e.toString())
    }

    override fun onProgressUpdate(progress: Int, total: Int) {
        println("pdf->" + progress)
    }

    override fun onSuccess(url: String?, destinationPath: String?) {
        println("pdf->" + destinationPath)
        adapter = PDFPagerAdapter(mContext, url?.substring(url?.lastIndexOf('/') + 1))
        remotePDFViewPager.adapter = adapter
        fl_container.removeAllViews()
        fl_container.addView(remotePDFViewPager)
    }

/* pdf--end*/

    override fun onDestroy() {
        super.onDestroy()
        adapter.close()
    }

}