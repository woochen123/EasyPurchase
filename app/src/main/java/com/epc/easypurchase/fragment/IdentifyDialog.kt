package com.epc.easypurchase.fragment

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.epc.easypurchase.R
import kotlinx.android.synthetic.main.dialog_take_photo.*


/**
 * Created by Administrator on 2018/9/5.
 */

open class IdentifyDialog : DialogFragment(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.dialog_fragment)
    }


    var onItemClick:OnItemClick? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(R.layout.dialog_take_photo, container, false)
        initView(rootView)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        tv_take_photo.text = getString(R.string.producer)
        tv_album.text = getString(R.string.customer)
    }


    private fun initView(rootView: View) {
        // 设置宽度为屏宽、靠近屏幕底部。
        val window = dialog.window
        val wlp = window!!.attributes
        wlp.gravity = Gravity.BOTTOM
        window.attributes = wlp
        val tv_cancel = rootView.findViewById<TextView>(R.id.tv_cancel)
        val tv_take_photo = rootView.findViewById<TextView>(R.id.tv_take_photo)
        val tv_album = rootView.findViewById<TextView>(R.id.tv_album)
        tv_cancel.setOnClickListener(this)
        tv_take_photo.setOnClickListener(this)
        tv_album.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tv_cancel -> dismiss()
            R.id.tv_take_photo ->{
                onItemClick?.click(tv_take_photo.text.toString())
                dismiss()
            }
            R.id.tv_album -> {
                onItemClick?.click(tv_album.text.toString())
                dismiss()
            }
        }
    }

    companion object {


        fun newInsatance(): IdentifyDialog {
            return IdentifyDialog()
        }
    }

    interface OnItemClick{
        fun click(content:String)
    }
}
