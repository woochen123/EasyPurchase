package com.epc.easypurchase.helper

import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 *
 * @author woochen
 * @time 2018/8/23 10:51
 */
object RxSchedulers {

    fun <U,D>io_main(): ObservableTransformer<U,D> {

        return ObservableTransformer { upstream ->
            upstream
                    .subscribeOn(Schedulers.io())
                    .unsubscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()) as ObservableSource<D>
        }
    }
}
