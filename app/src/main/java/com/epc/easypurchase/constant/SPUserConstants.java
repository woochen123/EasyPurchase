package com.epc.easypurchase.constant;

/**
 * Created by Administrator on 2018/8/27.
 */

public class SPUserConstants {
    public static final String USER_TOKEN = "user_token";
    public static final String USER_STATE = "user_state";
    public static final String USER_ID = "user_id";
}
