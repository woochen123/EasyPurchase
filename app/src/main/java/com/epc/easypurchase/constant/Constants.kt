package com.jiazhuo.blockgamesquare.constant

import android.widget.Toast

/**
 * Created by Administrator on 2018/11/9.
 */
object Constants {

    var toast: Toast? = null
    //pref_name
    val USER_PREF_NAME = "user_name"
    val GUIDE_PREF_NAME: String = "guide_pref"
    //other
    val WELCOME_PAGE = "welcome_page"
    val TYPE_SUPPLY = 3 //供应商
    val TYPE_PURCHASE = 4//采购商
}