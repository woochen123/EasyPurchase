package com.epc.easypurchase.activity

import android.text.TextUtils
import android.view.View
import com.epc.easypurchase.R
import com.epc.easypurchase.fragment.IdentifyDialog
import com.epc.easypurchase.mvp.contract.PasswordContract
import com.epc.easypurchase.mvp.contract.SmsCodeContract
import com.epc.easypurchase.mvp.presenter.PasswordPresenter
import com.epc.easypurchase.mvp.presenter.SmsCodePresenter
import com.epc.easypurchase.widget.CountDownTextView
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.jiazhuo.blockgamesquare.constant.Constants
import com.jiazhuo.blockgamesquare.util.toast
import com.woochen.baselibrary.base.mvp.BaseMvpActivity
import com.woochen.baselibrary.mvp.InjectPresenter
import com.woochen.baselibrary.util.StatusBarUtil
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function4
import kotlinx.android.synthetic.main.activity_forget_pwd.*
import java.util.concurrent.TimeUnit

class ForgetPwdActivity : BaseMvpActivity(), View.OnClickListener, PasswordContract.IPasswordView, SmsCodeContract.ISmsCodeView {
    override fun passwordSucess() {
        finish()
    }

    override fun onClick(v: View?) {
        when (v) {
            tv_back -> back()
            tv_identify -> {
                val takePhotoDialog = IdentifyDialog.newInsatance()
                takePhotoDialog.onItemClick = object : IdentifyDialog.OnItemClick {
                    override fun click(content: String) {
                        tv_identify.text = content
                    }

                }
                takePhotoDialog.show(supportFragmentManager, "identify")
            }
        }
    }

    @InjectPresenter
    lateinit var mPasswordPresenter: PasswordPresenter

    @InjectPresenter
    lateinit var mSmsCodePresenter: SmsCodePresenter

    override fun setContentView(): Int = R.layout.activity_forget_pwd

    override fun initData() {
        StatusBarUtil.setStatusBarFullTransparent(this)
        StatusBarUtil.StatusBarLightMode(this)
        initListener()
        initRegister()
    }

    private fun initRegister() {
        val observableName = RxTextView.textChanges(et_phone)
        val observableCode = RxTextView.textChanges(et_code)
        val observablePassword = RxTextView.textChanges(et_pwd)
        val observablePasswordAgain = RxTextView.textChanges(et_pwd_agin)
        val combineDisposable = Observable.combineLatest(
                observableName,
                observablePassword,
                observableCode,
                observablePasswordAgain,
                Function4<CharSequence, CharSequence, CharSequence, CharSequence, Boolean> { charSequence, charSequence2, charSequence3, charSequence4 ->
                    !TextUtils.isEmpty(charSequence) && !TextUtils.isEmpty(charSequence2)
                            && !TextUtils.isEmpty(charSequence2) && !TextUtils.isEmpty(charSequence3)
                            && !TextUtils.isEmpty(charSequence4)
                }
        )
                .subscribe { aBoolean -> btn_confirm.isEnabled = aBoolean!! }
        val clickDisposable = RxView.clicks(btn_confirm)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    register()
                }
        mPasswordPresenter.addDisposable(combineDisposable)
        mPasswordPresenter.addDisposable(clickDisposable)
    }


    private fun register() {
        val identifyName = tv_identify.text.toString()
        if (TextUtils.isEmpty(identifyName)) {
            toast("请选择您的身份类型！")
            return
        }

        val phone = et_phone.text.toString()
        val code = et_code.text.toString()
        val pwd = et_pwd.text.toString()
        val pwdAgaian = et_pwd_agin.text.toString()
        val type: Int = when (identifyName) {
            getString(R.string.producer) -> Constants.TYPE_SUPPLY
            getString(R.string.customer) -> Constants.TYPE_PURCHASE
            else -> 0
        }
        if (!TextUtils.equals(pwd, pwdAgaian)) {
            toast("两次输入的密码不一致！")
            return
        }
        mPasswordPresenter.forgetPassword(type, phone, code, pwd)
    }

    private fun initListener() {
        tv_back.setOnClickListener(this)
        tv_identify.setOnClickListener(this)
        cdtv_get_code.setCodeOnclickListener(object : CountDownTextView.CodeOnclickListener(cdtv_get_code) {
            override fun sendCode(): Boolean {
                val mobile = et_phone.text.toString().trim()
                if (TextUtils.isEmpty(mobile) || mobile.length != 11) {
                    toast("请输入正确的手机号")
                    return false
                }
                mSmsCodePresenter.sendCode(mobile)
                return true
            }

        })
    }


}
