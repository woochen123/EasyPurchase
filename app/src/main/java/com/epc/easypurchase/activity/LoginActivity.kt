package com.epc.easypurchase.activity

import android.text.TextUtils
import android.view.View
import com.epc.easypurchase.R
import com.epc.easypurchase.bean.UserBean
import com.epc.easypurchase.fragment.IdentifyDialog
import com.epc.easypurchase.mvp.contract.LoginContract
import com.epc.easypurchase.mvp.presenter.LoginPresenter
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import com.jiazhuo.blockgamesquare.constant.Constants
import com.jiazhuo.blockgamesquare.util.toast
import com.woochen.baselibrary.base.mvp.BaseMvpActivity
import com.woochen.baselibrary.mvp.InjectPresenter
import com.woochen.baselibrary.util.StatusBarUtil
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import kotlinx.android.synthetic.main.activity_login.*
import java.util.concurrent.TimeUnit

class LoginActivity : BaseMvpActivity(), View.OnClickListener, LoginContract.ILoginView {
    @InjectPresenter
    lateinit var mLoginPresenter: LoginPresenter

    private var type: Int = 0

    override fun setContentView(): Int = R.layout.activity_login

    override fun initData() {
        StatusBarUtil.setStatusBarFullTransparent(this)
        StatusBarUtil.StatusBarLightMode(this)
        initListener()
        initLogin()
    }

    override fun loginSucess(data: UserBean?) {
        val state = data?.state
       if (!TextUtils.isEmpty(data?.epc_token)){
           MainActivity.start(mContext, type)
       }
        when (state) {
            1-> {
                toast("您还没有完善信息！")
                CompleteInformationActivity.start(mContext,type) }
            2 -> {
                toast("您还没有完善信息！")
                CompleteInformationActivity.start(mContext,type)
            }
            3 -> {
                toast("您的信息正在审核中！")
                MainActivity.start(mContext, type)
            }
            4 -> {
                toast("您的账户已被禁用！")
                MainActivity.start(mContext, type)
            }
            5 -> {
                MainActivity.start(mContext, type)
            }
            6 -> {
                toast("您的信息审核失败，请重新提交！")
                CompleteInformationActivity.start(mContext,type)
            }
        }
        finish()
    }


    /**
     * 登录判断
     */
    private fun initLogin() {
        val observableName = RxTextView.textChanges(et_phone)
        val observablePassword = RxTextView.textChanges(et_pwd)
        val combineDisposable = Observable.combineLatest(observableName, observablePassword,
                BiFunction<CharSequence, CharSequence, Boolean>
                { charSequence, charSequence2 ->
                    !TextUtils.isEmpty(charSequence) && !TextUtils.isEmpty(charSequence2)
                })
                .subscribe { aBoolean -> btn_login.isEnabled = aBoolean!! }
        val clickDisposable = RxView.clicks(btn_login)
                .throttleFirst(1, TimeUnit.SECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    login()
                }
        mLoginPresenter.addDisposable(combineDisposable)
        mLoginPresenter.addDisposable(clickDisposable)
    }




    private fun login() {
        val identifyName = tv_identify.text.toString()
        if (TextUtils.isEmpty(identifyName)) {
            toast("请选择您的身份类型！")
            return
        }
        val phone = et_phone.text.toString()
        val pwd = et_pwd.text.toString()
         type = when (identifyName) {
            getString(R.string.producer) -> Constants.TYPE_SUPPLY
            getString(R.string.customer) -> Constants.TYPE_PURCHASE
            else -> 0
        }
        mLoginPresenter.login(phone, pwd, type)
    }


    private fun initListener() {
        tv_identify.setOnClickListener(this)
        tv_register.setOnClickListener(this)
        tv_forget_pwd.setOnClickListener(this)
        btn_login.setOnClickListener(this)
        iv_back.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v) {
            tv_identify -> {
                val takePhotoDialog = IdentifyDialog.newInsatance()
                takePhotoDialog.onItemClick = object : IdentifyDialog.OnItemClick {
                    override fun click(content: String) {
                        tv_identify.text = content
                    }

                }
                takePhotoDialog.show(supportFragmentManager, "identify")
            }
            tv_register -> startActivity(RegisterActivity::class.java)
            tv_forget_pwd -> startActivity(ForgetPwdActivity::class.java)
            btn_login -> login()
            iv_back -> back()
        }
    }


}
