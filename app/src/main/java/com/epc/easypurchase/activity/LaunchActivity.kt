package com.epc.easypurchase.activity

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import com.bigkoo.convenientbanner.holder.CBViewHolderCreator
import com.bigkoo.convenientbanner.holder.Holder
import com.bigkoo.convenientbanner.listener.OnPageChangeListener
import com.epc.easypurchase.R
import com.epc.easypurchase.bean.GuideBean
import com.epc.easypurchase.constant.SPUserConstants
import com.epc.easypurchase.service.PersonPushIntentService
import com.epc.easypurchase.service.PersonPushService
import com.epc.easypurchase.util.SPUserUtil
import com.epc.easypurchase.util.SPUtil
import com.epc.easypurchase.widget.SplashView
import com.igexin.sdk.PushManager
import com.jiazhuo.blockgamesquare.constant.Constants
import com.jiazhuo.blockgamesquare.util.ResourceUtil
import com.woochen.baselibrary.base.mvp.BaseMvpActivity
import com.woochen.baselibrary.util.StatusBarUtil
import kotlinx.android.synthetic.main.activity_launch.*


class LaunchActivity : BaseMvpActivity() {

    override fun setContentView(): Int = R.layout.activity_launch

    override fun initData() {
        initPush()
        StatusBarUtil.setStatusBarFullTransparent(this)
        StatusBarUtil.StatusBarLightMode(this)
        if (intent.flags and Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT !== 0) {
            finish()
            return
        }
        initSplash()
    }


    private fun initPush() {
        PushManager.getInstance().initialize(this.applicationContext, PersonPushService::class.java)
        PushManager.getInstance().registerPushIntentService(this.applicationContext, PersonPushIntentService::class.java)
    }

    private fun initSplash() {
        SplashView.showSplashView(this, 2, R.drawable.ic_launch, object : SplashView.OnSplashViewActionListener {
            override fun onSplashImageClick(actionUrl: String) {
            }

            override fun onSplashViewDismiss(initiativeDismiss: Boolean) {
                toJump()
            }
        })

        // call this method anywhere to update splash view data
//        SplashView.updateSplashData(this, "http://ww2.sinaimg.cn/large/72f96cbagw1f5mxjtl6htj20g00sg0vn.jpg", "http://jkyeo.com")
    }




    /**
     * 初始化引导页
     */
    private fun initGuidePage() {
        var localImages = mutableListOf<GuideBean>()
        localImages.add(GuideBean(R.drawable.ic_guide1, ResourceUtil.getString(R.string.desc_guide1)))
        localImages.add(GuideBean(R.drawable.ic_guide2, ResourceUtil.getString(R.string.desc_guide2)))
        localImages.add(GuideBean(R.drawable.ic_guide3, ResourceUtil.getString(R.string.desc_guide3)))
        val indicators = intArrayOf(R.drawable.rec_btn_unfoucus, R.drawable.rec_btn_foucus)
        convenientBanner.setPages(object : CBViewHolderCreator {
            override fun createHolder(itemView: View?): Holder<*> = LocalImageHolderView(itemView!!)

            override fun getLayoutId(): Int = R.layout.item_localimage
        }, localImages as List<Nothing>)
                .setPageIndicator(indicators)
//                .setOnItemClickListener(OnItemClickListener {  })
                .onPageChangeListener = object : OnPageChangeListener {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {

            }

            override fun onPageSelected(index: Int) {
                btn_guide_enter.visibility = if (index == localImages.size - 1) {
                    View.VISIBLE
                } else View.GONE
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {

            }
        }

    }

    class LocalImageHolderView : Holder<GuideBean> {
        constructor(itemView: View) : super(itemView)

        private var imageView: ImageView? = null
        private var textView: TextView? = null
        override fun updateUI(data: GuideBean?) {
            imageView!!.setImageResource(data?.pic!!)
        }

        override fun initView(itemView: View?) {
            imageView = itemView?.findViewById(R.id.iv_pic)
            textView = itemView?.findViewById(R.id.tv_desc)
        }

    }

    private fun toJump() {
        val isGuide = SPUtil.getString(SPUtil.getSP(this@LaunchActivity, Constants.GUIDE_PREF_NAME), Constants.WELCOME_PAGE)
        if (TextUtils.equals("1", isGuide)) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            toMainActivity(null)
        } else {
            initGuidePage()
        }
    }


    fun toMainActivity(view: View?) {
        SPUtil.putString(SPUtil.getSP(this@LaunchActivity, Constants.GUIDE_PREF_NAME), Constants.WELCOME_PAGE,"1");
       /* if (TextUtils.isEmpty(SPUserUtil.getString(SPUserConstants.USER_TOKEN))){
            startActivity(LoginActivity::class.java)
        }else {
            MainActivity.start(mContext,Constants.TYPE_SUPPLY)
        }*/
        MainActivity.start(mContext,Constants.TYPE_SUPPLY)
        finish()
    }


}
