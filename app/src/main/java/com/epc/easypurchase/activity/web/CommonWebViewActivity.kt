package com.epc.easypurchase.activity.web

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.webkit.JavascriptInterface
import com.epc.easypurchase.R
import com.epc.easypurchase.activity.LoginActivity
import com.epc.easypurchase.activity.MainActivity


import com.epc.easypurchase.bean.SerializableMap
import com.epc.easypurchase.constant.SPUserConstants
import com.epc.easypurchase.fragment.PrePdfDialog
import com.epc.easypurchase.mvp.contract.LogoutContract
import com.epc.easypurchase.mvp.presenter.LogoutPresenter
import com.epc.easypurchase.util.SPUserUtil
import com.epc.easypurchase.util.SimpleDialogUtil
import com.jiazhuo.blockgamesquare.util.toast
import com.skateboard.zxinglib.CaptureActivity
import com.tbruyelle.rxpermissions2.RxPermissions
import com.woochen.baselibrary.mvp.InjectPresenter
import kotlinx.android.synthetic.main.activity_common_web_view.*

import java.util.HashMap

/**
 * 通用webview
 *
 * @author woochen
 * @time 2018/8/31 10:27
 */
class CommonWebViewActivity : BaseWebViewActivity(), LogoutContract.ILogoutView {
    private var mUrl: String? = ""
    private val SCAN_REQUEST_CODE = 0x0010
    private val LOGIN_REQUEST_CODE = 0x0011

    @InjectPresenter
    lateinit var mLogoutPresenter: LogoutPresenter
    lateinit var rxPermissions: RxPermissions

    override fun logoutSucess() {
        startActivity(MainActivity::class.java)
    }

    override fun initData() {
        super.initData()
        rxPermissions = RxPermissions(this)
        val intent = intent
        if (intent != null) {
            mUrl = intent.getStringExtra(LINK_URL)
            val serializableMap = intent.getSerializableExtra(MAP_PARAM)
        }
        val headParams = hashMapOf<String, String>()
        headParams.put("epc-token", SPUserUtil.getString(SPUserConstants.USER_TOKEN)!!)
        web.loadUrl(mUrl, headParams)
    }

    companion object {
        private val TITLE = "web_title"
        private val LINK_URL = "link_url"
        private val MAP_PARAM = "map_param"


        /**
         * 启动
         */
        fun start(context: Context, title: String, url: String) {
            val intent = Intent(context, CommonWebViewActivity::class.java)
            intent.putExtra(TITLE, title)
            intent.putExtra(LINK_URL, url)
            context.startActivity(intent)
        }

        fun start(context: Context, url: String) {
            val intent = Intent(context, CommonWebViewActivity::class.java)
            intent.putExtra(LINK_URL, url)
            context.startActivity(intent)
        }


        /**
         * 启动
         *
         * @param context
         * @param title
         * @param url
         * @param map     需要传递给web的参数
         */
        fun start(context: Context, title: String, url: String, map: HashMap<String, String>) {
            val serializableMap = SerializableMap()
            serializableMap.map = map
            val intent = Intent(context, CommonWebViewActivity::class.java)
            intent.putExtra(TITLE, title)
            intent.putExtra(LINK_URL, url)
            intent.putExtra(MAP_PARAM, serializableMap)
            context.startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SCAN_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val result = data.getStringExtra(CaptureActivity.KEY_DATA)
            toast("qrcode result is $result")
        }else if (requestCode == LOGIN_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            finish()
        }
    }


    /**
     * 退出登录
     */
    @JavascriptInterface
    fun logout() {
        SimpleDialogUtil.confirmAndCancel(mContext, R.string.tip_logout, R.string.cancle, R.string.confirm,
                object : SimpleDialogUtil.SimpleClickListener {
                    override fun confirm() {
                        mLogoutPresenter.logout()
                    }

                })
    }


    /**
     * 打开新activity
     */
    @JavascriptInterface
    fun openNewPage(url: String) {
        CommonWebViewActivity.start(mContext, url)
    }


    /**
     * 关闭activity
     */
    @JavascriptInterface
    fun closePage() {
        finish()
    }

    /**
     * 扫一扫
     */
    @JavascriptInterface
    fun scanCode() {
        runOnUiThread {
            rxPermissions
                    .request(Manifest.permission.CAMERA)
                    .subscribe({ granted ->
                        if (granted) {
                            println("已授权")
                            val intent = Intent(mContext, CaptureActivity::class.java)
                            startActivityForResult(intent, SCAN_REQUEST_CODE)
                        } else {
                            println("缺少相机权限！")
                            toast("缺少相机权限！")
                        }
                    })

        }

    }

    /**
     * 预览pdf
     */
    @JavascriptInterface
    fun previewPdf(pdfUrl: String) {
        val prePdfDialog = PrePdfDialog.newInsatance(pdfUrl)
        prePdfDialog.show(supportFragmentManager, "prePdf")

    }


    /**
     * 登录
     */
    @JavascriptInterface
    fun login(){
        startActivity(LoginActivity::class.java)
    }


}
