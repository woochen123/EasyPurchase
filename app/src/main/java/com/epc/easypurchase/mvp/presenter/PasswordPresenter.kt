package com.epc.easypurchase.mvp.presenter


import com.epc.easypurchase.mvp.contract.PasswordContract
import com.epc.easypurchase.mvp.model.UserModel
import com.jiazhuo.blockgamesquare.http.callback.BaseObserver
import com.jiazhuo.blockgamesquare.http.callback.CustomException
import com.woochen.baselibrary.base.mvp.BasePresenter

/**
 * Created by Administrator on 2018/8/21.
 */

class PasswordPresenter : BasePresenter<PasswordContract.IPasswordView>(), PasswordContract.IPasswordPresenter {
    private var mUserModel: UserModel = UserModel()


    override fun forgetPassword(type: Int, mobile: String, verify_code: String, password: String) {
        val rxBaseObserver = object : BaseObserver<Any>() {
            override fun fail(e: CustomException) {

            }

            override fun success(data: Any?) {
                view.passwordSucess()
            }

        }
        mUserModel.forgetPwd(mobile, password,type,verify_code, rxBaseObserver)
        addDisposable(rxBaseObserver)
    }
}
