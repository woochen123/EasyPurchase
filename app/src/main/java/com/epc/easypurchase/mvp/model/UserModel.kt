package com.epc.easypurchase.mvp.model


import com.epc.easypurchase.bean.*
import com.epc.easypurchase.helper.RxSchedulers
import com.epc.easypurchase.util.RequestBodyUtil
import com.google.gson.Gson
import com.jiazhuo.blockgamesquare.bean.BaseBean

import io.reactivex.Observer

/**
 * 用户相关model
 * @author woochen
 * @time 2018/8/27 14:55
 */

class UserModel : BaseModel() {
    /**
     * 登录
     */
    fun login(account: String, password: String, type: Int, observable: Observer<BaseBean<UserBean>>) {
        val loginBean = LoginBean(account, password, type, "1")
        rxApi.login(toParam(loginBean))
                .compose<BaseBean<UserBean>>(RxSchedulers.io_main())
                .subscribe(observable)
    }

    /**
     * 注册
     */
    fun register(account: String, nickname: String, password: String, type: Int, verityCode: String, observable: Observer<BaseBean<Any>>) {
        val loginBean = RegisterBean(account, nickname, password, type, verityCode)
        rxApi.register(toParam(loginBean))
                .compose<BaseBean<Any>>(RxSchedulers.io_main())
                .subscribe(observable)
    }

    /**
     * 忘记密码
     */
    fun forgetPwd(account: String, password: String, type: Int, verityCode: String, observable: Observer<BaseBean<Any>>) {
        val loginBean = PasswordBean(account, password, type, verityCode)
        rxApi.forgetPwd(toParam(loginBean))
                .compose<BaseBean<Any>>(RxSchedulers.io_main())
                .subscribe(observable)
    }

    /**
     * 退出登录
     */
    fun logout(observable: Observer<BaseBean<Any>>) {
        rxApi.logout(token)
                .compose<BaseBean<Any>>(RxSchedulers.io_main())
                .subscribe(observable)
    }

    /**
     * 发送验证码
     */
    fun sendCode(phone: String, observable: Observer<BaseBean<Any>>) {
        rxApi.sendCode(phone)
                .compose<BaseBean<Any>>(RxSchedulers.io_main())
                .subscribe(observable)
    }


    /**
     * 获取上传凭证
     */
    fun getUploadToken(fileName: String, observable: Observer<BaseBean<UploadResponseBean>>) {
        val uploadBean = UploadBean("user_info", fileName)
        rxApi.getUploadToken(toParam(uploadBean))
                .compose<BaseBean<UploadResponseBean>>(RxSchedulers.io_main())
                .subscribe(observable)
    }

    /**
     * 完善采购人信息
     */
    fun completePurchaseData(accountOpening: String, // 开户照片
                             area: String, // 区
                             certificateFilePath: String, // 资质文件url
                             certificateName: String, // 资质文件名称
                             businessLicense: String, // 营业执照照片url
                             certificateOfAuthorization: String, // 带公章的授权书照片url
                             city: String, // 市
                             companyAddress: String, // 公司详细地址
                             companyName: String, // 公司名称
                             legalIdCardOther: String, // 法人身份证反面
                             legalIdCardPositive: String, // 法人身份证正面
                             legalIdCardPositiveNumber: String, // 法人身份证号码
                             province: String, // 省
                             publicBankCount: String, // 开户银行账号
                             publicBankName: String, // 开户银行名称
                             uniformCreditCode: String, // 统一信用代码
                             uid: String, //
                             observable: Observer<BaseBean<Any>>) {
        val att = PurChaseBean.Att(certificateFilePath, certificateName, "", "")
        val atts = mutableListOf<PurChaseBean.Att>(att)
        val purChaseBean = PurChaseBean(accountOpening, area, atts, businessLicense, certificateOfAuthorization, city,
                companyAddress, companyName, legalIdCardOther, legalIdCardPositive,
                legalIdCardPositiveNumber, province, publicBankCount, publicBankName, uniformCreditCode,uid,"4")
        rxApi.completePurchaseData(toParam(purChaseBean))
                .compose<BaseBean<Any>>(RxSchedulers.io_main())
                .subscribe(observable)
    }

    /**
     * 完善供应商资料
     */
    fun completeSupplyData(accountOpening: String, // 开户证明
                           area: String, // 区
                           businessLicense: String, // 营业执照照片url
                           certificateOfAuthorization: String, // 带公章的授权书照片url
                           city: String, // 市
                           companyAddress: String, // 公司详细地址
                           companyName: String, // 公司名称
                           legalIdCardOther: String, // 法人身份证反面
                           legalIdCardPositive: String, // 法人身份证正面
                           legalIdCardPositiveNumber: String, // 法人身份证号码
                           province: String, // 省
                           publicBanAccountNumber: String, // 开户银行账号
                           publicBankName: String, // 开户银行名称
                           qualificationCertificate: String, // 资质证书url
                           qualificationCertificateNumber: String, // 资质证书号码
                           uniformCreditCode: String ,// 统一信用代码
                           uid: String //
                           , observable: Observer<BaseBean<Any>>) {
        val qc = SupplyBean.Qc(qualificationCertificate, qualificationCertificateNumber)
        val qcs = mutableListOf<SupplyBean.Qc>(qc)
        val supplyBean = SupplyBean(accountOpening, area, businessLicense, certificateOfAuthorization, city, companyAddress, companyName,
                legalIdCardOther, legalIdCardPositive, legalIdCardPositiveNumber, province, publicBanAccountNumber, publicBankName,
                qcs, uniformCreditCode,uid)
        rxApi.completeSupplyData(toParam(supplyBean))
                .compose<BaseBean<Any>>(RxSchedulers.io_main())
                .subscribe(observable)
    }


}
