package com.epc.easypurchase.mvp.presenter


import com.epc.easypurchase.mvp.contract.SmsCodeContract
import com.epc.easypurchase.mvp.model.UserModel
import com.jiazhuo.blockgamesquare.http.callback.BaseObserver
import com.jiazhuo.blockgamesquare.http.callback.CustomException
import com.woochen.baselibrary.base.mvp.BasePresenter

/**
 * Created by Administrator on 2018/8/21.
 */

class SmsCodePresenter : BasePresenter<SmsCodeContract.ISmsCodeView>(), SmsCodeContract.ISmsCodePresenter {
    private var mUserModel: UserModel = UserModel()

    override fun sendCode(mobile: String) {
        val rxBaseObserver = object : BaseObserver<Any>() {
            override fun fail(e: CustomException) {

            }

            override fun success(data: Any?) {
            }
        }
        mUserModel.sendCode(mobile, rxBaseObserver)
        addDisposable(rxBaseObserver)
    }
}
