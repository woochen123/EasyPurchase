package com.epc.easypurchase.mvp.contract

import com.woochen.baselibrary.mvp.IBaseView

/**
 * 登录
 * @author woochen
 * @time 2018/8/27 14:54
 */

class LogoutContract {
    interface ILogoutPresenter {
        fun logout()
    }

    interface ILogoutView : IBaseView {
        fun logoutSucess()
    }
}
