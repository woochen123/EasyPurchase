package com.epc.easypurchase.mvp.contract

import com.epc.easypurchase.bean.UploadResponseBean
import com.woochen.baselibrary.mvp.IBaseView

/**
 * 注册
 * @author woochen
 * @time 2018/8/27 14:54
 */

class UploadTokenContract {
    interface IUploadTokenePresenter {
        fun getToken(fileName: String)
    }

    interface IUploadTokeneView : IBaseView{
        fun getTokenSucess(result:UploadResponseBean)
    }
}