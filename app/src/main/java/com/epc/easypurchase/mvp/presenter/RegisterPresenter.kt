package com.epc.easypurchase.mvp.presenter


import com.epc.easypurchase.R
import com.epc.easypurchase.bean.UserBean
import com.epc.easypurchase.mvp.contract.RegisterContract
import com.epc.easypurchase.mvp.model.UserModel
import com.jiazhuo.blockgamesquare.http.callback.BaseObserver
import com.jiazhuo.blockgamesquare.http.callback.CustomException
import com.woochen.baselibrary.base.mvp.BasePresenter

import java.util.HashMap

/**
 * Created by Administrator on 2018/8/21.
 */

class RegisterPresenter : BasePresenter<RegisterContract.IRegisterView>(), RegisterContract.IRegisterPresenter {
    private var mUserModel: UserModel = UserModel()

    override fun register(mobile: String,nickname: String, pwd: String, type: Int, verify_code: String) {
        val rxBaseObserver = object : BaseObserver<Any>() {
            override fun onStart() {
                view.showLoading(true)
            }

            override fun success(data: Any?) {
                view.registerSucess()
                view.showContent()
            }

            override fun fail(e: CustomException) {
                view.showContent()
            }
        }
        mUserModel.register(mobile,nickname,pwd,type,verify_code, rxBaseObserver)
        addDisposable(rxBaseObserver)
    }


}
