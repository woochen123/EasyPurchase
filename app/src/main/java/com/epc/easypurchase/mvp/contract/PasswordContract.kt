package com.epc.easypurchase.mvp.contract

import com.woochen.baselibrary.mvp.IBaseView

/**
 * 修改密码/忘记密码
 * @author woochen
 * @time 2018/8/27 14:54
 */

class PasswordContract {
    interface IPasswordPresenter {
        fun forgetPassword(type:Int,mobile: String, verify_code: String, password: String) //忘记密码
    }

    interface IPasswordView : IBaseView {
        fun passwordSucess()
    }
}
