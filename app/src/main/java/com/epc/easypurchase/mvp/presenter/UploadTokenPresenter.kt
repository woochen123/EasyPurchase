package com.epc.easypurchase.mvp.presenter

import com.epc.easypurchase.bean.UploadResponseBean
import com.epc.easypurchase.mvp.contract.SmsCodeContract
import com.epc.easypurchase.mvp.contract.UploadTokenContract
import com.epc.easypurchase.mvp.model.UserModel
import com.jiazhuo.blockgamesquare.http.callback.BaseObserver
import com.jiazhuo.blockgamesquare.http.callback.CustomException
import com.jiazhuo.blockgamesquare.util.toast
import com.woochen.baselibrary.base.mvp.BasePresenter

/**
 * Created by Administrator on 2018/8/21.
 */

class UploadTokenPresenter : BasePresenter<UploadTokenContract.IUploadTokeneView>(), UploadTokenContract.IUploadTokenePresenter {
    override fun getToken(fileName: String) {
        val rxBaseObserver = object : BaseObserver<UploadResponseBean>() {
            override fun fail(e: CustomException) {
//                toast("上传失败！code -> 10001")
                view.showContent()
            }

            override fun success(data: UploadResponseBean?) {
                view.getTokenSucess(data!!)
            }
        }
        mUserModel.getUploadToken(fileName, rxBaseObserver)
        addDisposable(rxBaseObserver)
    }

    private var mUserModel: UserModel = UserModel()


}