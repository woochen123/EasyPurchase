package com.epc.easypurchase.mvp.presenter


import com.epc.easypurchase.R
import com.epc.easypurchase.mvp.contract.LogoutContract
import com.epc.easypurchase.mvp.model.UserModel
import com.epc.easypurchase.util.SPUserUtil
import com.jiazhuo.blockgamesquare.http.callback.BaseObserver
import com.jiazhuo.blockgamesquare.http.callback.CustomException
import com.woochen.baselibrary.base.mvp.BasePresenter

/**
 * Created by Administrator on 2018/8/21.
 */

class LogoutPresenter : BasePresenter<LogoutContract.ILogoutView>(), LogoutContract.ILogoutPresenter {
    private var mUserModel: UserModel = UserModel()

    override fun logout() {
        val rxBaseObserver = object : BaseObserver<Any>() {
            override fun onStart() {
                view.showLoading(true)
            }

            override fun success(data: Any?) {
                view.logoutSucess()
                view.showContent()
                //clear data
                SPUserUtil.clearAll()
            }

            override fun fail(e: CustomException) {
                view.showContent()
            }

        }
        mUserModel.logout(rxBaseObserver)
        addDisposable(rxBaseObserver)
    }
}
