package com.epc.easypurchase.mvp.model


import com.jiazhuo.blockgamesquare.http.api.ApiService
import okhttp3.RequestBody

/**
 * Created by Administrator on 2018/8/23.
 */

interface IBaseModel {
    val rxApi: ApiService
    fun toParam(any: Any): RequestBody
    val token:String
}
