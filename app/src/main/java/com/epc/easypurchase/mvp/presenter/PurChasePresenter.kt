package com.epc.easypurchase.mvp.presenter

import com.epc.easypurchase.bean.UploadResponseBean
import com.epc.easypurchase.mvp.contract.PurChaseContract
import com.epc.easypurchase.mvp.contract.UploadTokenContract
import com.epc.easypurchase.mvp.model.UserModel
import com.jiazhuo.blockgamesquare.http.callback.BaseObserver
import com.jiazhuo.blockgamesquare.http.callback.CustomException
import com.woochen.baselibrary.base.mvp.BasePresenter

/**
 * Created by Administrator on 2018/8/21.
 */

class PurChasePresenter : BasePresenter<PurChaseContract.IPurChaseView>(), PurChaseContract.IPurChasePresenter {
    override fun completePurChase(accountOpening: String, area: String, certificateFilePath: String, certificateName: String, businessLicense: String, certificateOfAuthorization: String, city: String, companyAddress: String, companyName: String, legalIdCardOther: String, legalIdCardPositive: String, legalIdCardPositiveNumber: String, province: String, publicBankCount: String, publicBankName: String, uniformCreditCode: String,uid: String) {
        val rxBaseObserver = object : BaseObserver<Any>() {
            override fun fail(e: CustomException) {
            }

            override fun success(data: Any?) {
                view.completePurChaseSucess()
            }
        }
        mUserModel.completePurchaseData(accountOpening, area, certificateFilePath,
                certificateName, businessLicense, certificateOfAuthorization, city, companyAddress,
                companyName, legalIdCardOther, legalIdCardPositive, legalIdCardPositiveNumber, province,
                publicBankCount, publicBankName, uniformCreditCode,uid,rxBaseObserver)
        addDisposable(rxBaseObserver)
    }


    private var mUserModel: UserModel = UserModel()


}