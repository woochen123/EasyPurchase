package com.epc.easypurchase.mvp.contract

import com.epc.easypurchase.bean.UploadResponseBean
import com.woochen.baselibrary.mvp.IBaseView

/**
 * 注册
 * @author woochen
 * @time 2018/8/27 14:54
 */

class PurChaseContract {
    interface IPurChasePresenter {
        fun completePurChase(accountOpening: String, // 开户照片
                             area: String, // 区
                             certificateFilePath: String, // 资质文件url
                             certificateName: String, // 资质文件名称
                             businessLicense: String, // 营业执照照片url
                             certificateOfAuthorization: String, // 带公章的授权书照片url
                             city: String, // 市
                             companyAddress: String, // 公司详细地址
                             companyName: String, // 公司名称
                             legalIdCardOther: String, // 法人身份证反面
                             legalIdCardPositive: String, // 法人身份证正面
                             legalIdCardPositiveNumber: String, // 法人身份证号码
                             province: String, // 省
                             publicBankCount: String, // 开户银行账号
                             publicBankName: String, // 开户银行名称
                             uniformCreditCode: String ,// 统一信用代码
                             uid: String //
                 )
    }

    interface IPurChaseView : IBaseView {
        fun completePurChaseSucess()
    }
}