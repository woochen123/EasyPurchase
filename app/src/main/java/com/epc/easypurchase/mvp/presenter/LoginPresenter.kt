package com.epc.easypurchase.mvp.presenter


import android.text.TextUtils
import com.epc.easypurchase.bean.UserBean
import com.epc.easypurchase.constant.SPUserConstants
import com.epc.easypurchase.mvp.contract.LoginContract
import com.epc.easypurchase.mvp.model.UserModel
import com.epc.easypurchase.util.SPUserUtil
import com.jiazhuo.blockgamesquare.http.callback.BaseObserver
import com.jiazhuo.blockgamesquare.http.callback.CustomException
import com.woochen.baselibrary.base.mvp.BasePresenter

/**
 * Created by Administrator on 2018/8/21.
 */

class LoginPresenter : BasePresenter<LoginContract.ILoginView>(), LoginContract.ILoginPresenter {
    private var mUserModel: UserModel = UserModel()

    override fun login(account: String, password: String, type: Int) {
        val rxBaseObserver = object : BaseObserver<UserBean>() {
            override fun onStart() {
                view.showLoading(true)
            }

            override fun success(data: UserBean?) {
                view.loginSucess(data)
                view.showContent()
                if (!TextUtils.isEmpty(data?.epc_token)){
                    SPUserUtil.putString(SPUserConstants.USER_TOKEN, data?.epc_token!!)
                }
                if (!TextUtils.isEmpty(data?.userId)){
                    SPUserUtil.putInt(SPUserConstants.USER_STATE, data?.state!!)
                    SPUserUtil.putString(SPUserConstants.USER_ID, data.userId)
                }

            }

            override fun fail(e: CustomException) {
                view.showContent()
            }

        }
        mUserModel.login(account, password, type, rxBaseObserver)
        addDisposable(rxBaseObserver)
    }
}
