package com.epc.easypurchase.mvp.model


import com.epc.easypurchase.constant.SPUserConstants
import com.epc.easypurchase.util.RequestBodyUtil
import com.epc.easypurchase.util.SPUserUtil
import com.google.gson.Gson
import com.jiazhuo.blockgamesquare.http.RetrofitUtil
import com.jiazhuo.blockgamesquare.http.api.ApiService
import okhttp3.RequestBody

/**
 * Created by Administrator on 2018/8/23.
 */

open class BaseModel : IBaseModel {
    override val token: String
        get() = SPUserUtil.getString(SPUserConstants.USER_TOKEN)!!
    override val rxApi: ApiService
        get() = RetrofitUtil.api!!


    override fun toParam(any: Any):RequestBody = RequestBodyUtil.convertToRequestBody(Gson().toJson(any))
}
