package com.epc.easypurchase.mvp.contract

import com.woochen.baselibrary.mvp.IBaseView

/**
 * 注册
 * @author woochen
 * @time 2018/8/27 14:54
 */

class SupplyContract {
    interface ISupplyPresenter {
        fun completeSupply(accountOpening: String, // 开户证明
                           area: String, // 区
                           businessLicense: String, // 营业执照照片url
                           certificateOfAuthorization: String, // 带公章的授权书照片url
                           city: String, // 市
                           companyAddress: String, // 公司详细地址
                           companyName: String, // 公司名称
                           legalIdCardOther: String, // 法人身份证反面
                           legalIdCardPositive: String, // 法人身份证正面
                           legalIdCardPositiveNumber: String, // 法人身份证号码
                           province: String, // 省
                           publicBanAccountNumber: String, // 开户银行账号
                           publicBankName: String, // 开户银行名称
                           qualificationCertificate: String, // 资质证书url
                           qualificationCertificateNumber: String, // 资质证书号码
                           uniformCreditCode: String, // 统一信用代码
                           uid:String
                 )
    }

    interface ISupplyView : IBaseView {
        fun completeSupplySucess()
    }
}