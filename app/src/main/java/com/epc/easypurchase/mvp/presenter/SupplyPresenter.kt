package com.epc.easypurchase.mvp.presenter

import com.epc.easypurchase.mvp.contract.PurChaseContract
import com.epc.easypurchase.mvp.contract.SupplyContract
import com.epc.easypurchase.mvp.model.UserModel
import com.jiazhuo.blockgamesquare.http.callback.BaseObserver
import com.jiazhuo.blockgamesquare.http.callback.CustomException
import com.woochen.baselibrary.base.mvp.BasePresenter

/**
 * Created by Administrator on 2018/8/21.
 */

class SupplyPresenter : BasePresenter<SupplyContract.ISupplyView>(), SupplyContract.ISupplyPresenter {
    override fun completeSupply(accountOpening: String, area: String, businessLicense: String,
                                certificateOfAuthorization: String, city: String, companyAddress: String,
                                companyName: String, legalIdCardOther: String, legalIdCardPositive: String,
                                legalIdCardPositiveNumber: String, province: String, publicBanAccountNumber: String,
                                publicBankName: String, qualificationCertificate: String, qualificationCertificateNumber: String,
                                uniformCreditCode: String,uid:String) {
        val rxBaseObserver = object : BaseObserver<Any>() {
            override fun fail(e: CustomException) {
            }

            override fun success(data: Any?) {
                view.completeSupplySucess()
            }
        }
        mUserModel.completeSupplyData(accountOpening, area, businessLicense,
                certificateOfAuthorization, city, companyAddress, companyName,
                legalIdCardOther, legalIdCardPositive, legalIdCardPositiveNumber, province,
                publicBanAccountNumber, publicBankName,
                qualificationCertificate, qualificationCertificateNumber, uniformCreditCode,uid,rxBaseObserver)
        addDisposable(rxBaseObserver)
    }

    private var mUserModel: UserModel = UserModel()


}