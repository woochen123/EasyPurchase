package com.epc.easypurchase.mvp.contract

import com.epc.easypurchase.bean.UserBean
import com.woochen.baselibrary.mvp.IBaseView

/**
 * 登录
 * @author woochen
 * @time 2018/8/27 14:54
 */

class LoginContract {
    interface ILoginPresenter {
        fun login(account: String, password: String,type :Int)
    }

    interface ILoginView : IBaseView {
        fun loginSucess(data: UserBean?)
    }
}
