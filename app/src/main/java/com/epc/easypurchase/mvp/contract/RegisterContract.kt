package com.epc.easypurchase.mvp.contract

import com.woochen.baselibrary.mvp.IBaseView

/**
 * 注册
 * @author woochen
 * @time 2018/8/27 14:54
 */

class RegisterContract {
    interface IRegisterPresenter {
        fun register(mobile: String,nickname: String, pwd: String, type: Int, verify_code: String)
    }

    interface IRegisterView : IBaseView {
        fun registerSucess()
    }
}
