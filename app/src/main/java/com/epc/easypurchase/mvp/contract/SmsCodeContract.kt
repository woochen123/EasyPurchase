package com.epc.easypurchase.mvp.contract

import com.woochen.baselibrary.mvp.IBaseView

/**
 * 注册
 * @author woochen
 * @time 2018/8/27 14:54
 */

class SmsCodeContract {
    interface ISmsCodePresenter {
        fun sendCode(mobile: String)
    }

    interface ISmsCodeView : IBaseView
}
