package com.epc.easypurchase.bean

/**
 * Created by chenwuchao on 2018/11/24.
 */
data class RegisterBean(
        val cellphone:String,
        val name:String,
        val password:String,
        val type:Int,
        val verityCode:String
                      )