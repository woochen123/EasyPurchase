package com.epc.easypurchase.bean

data class SupplyBean(
        val accountOpening: String, // 开户证明
        val area: String, // 区
        val businessLicense: String, // 营业执照照片url
//        val businessLicenseNumber: String, // 营业执照号码
//        val categoryId: Int, // 供货商类别id
        val certificateOfAuthorization: String, // 带公章的授权书照片url
//        val certificateOfAuthorizationNumber: String, // 带公章的授权书号码
        val city: String, // 市
        val companyAddress: String, // 公司详细地址
        val companyName: String, // 公司名称
        val legalIdCardOther: String, // 法人身份证反面
        val legalIdCardPositive: String, // 法人身份证正面
        val legalIdCardPositiveNumber: String, // 法人身份证号码
//        val operatorIdCardFront: String, // 经办人(运营商员工)手持身份证正面照片url
//        val operatorIdCardFrontNumber: String, // 经办人(运营商员工)手持身份证号码
        val province: String, // 省
        val publicBanAccountNumber: String, // 开户银行账号
        val publicBankName: String, // 开户银行名称
        val qcs: List<Qc>,
        val uniformCreditCode: String ,// 统一信用代码
        val supplierId: String // 供应商id
) {
    data class Qc(
            val qualificationCertificate: String, // 资质证书url
            val qualificationCertificateNumber: String // 资质证书号码
    )
}