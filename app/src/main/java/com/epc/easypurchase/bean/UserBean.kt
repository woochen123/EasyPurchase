package com.epc.easypurchase.bean

import com.google.gson.annotations.SerializedName

/**
 * Created by chenwuchao on 2018/11/24.
 */
data class UserBean(
        @SerializedName("epc-token")
        val epc_token: String,

        val cellphone: String,
        val userId: String,
        val state: Int
)