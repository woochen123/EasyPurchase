package com.epc.easypurchase.bean

/**
 * Created by Administrator on 2018/11/22.
 */
data class GuideBean(
        val pic:Int,
        val desc:String
)