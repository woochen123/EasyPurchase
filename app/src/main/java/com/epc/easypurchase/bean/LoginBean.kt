package com.epc.easypurchase.bean

/**
 * Created by chenwuchao on 2018/11/24.
 */
data class LoginBean (
        val cellphone:String,
        val password:String,
        val type:Int,
        val verityCode:String
                      )