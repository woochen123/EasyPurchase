package com.epc.easypurchase.bean

data class PurChaseBean(
        val accountOpening: String, // 开户照片
        val area: String, // 区
        val atts: List<Att>,
        val businessLicense: String, // 营业执照照片url
        val certificateOfAuthorization: String, // 带公章的授权书照片url
        val city: String, // 市
        val companyAddress: String, // 公司详细地址
        val companyName: String, // 公司名称
        val legalIdCardOther: String, // 法人身份证反面
        val legalIdCardPositive: String, // 法人身份证正面
        val legalIdCardPositiveNumber: String, // 法人身份证号码
        val province: String, // 省
        val publicBankCount: String, // 开户银行账号
        val publicBankName: String, // 开户银行名称
        val uniformCreditCode: String ,// 统一信用代码
        val purchaseId: String, // 采购人id
        val userType: String // 用户类型
) {
    data class Att(
            //资质文件
            val certificateFilePath: String, // 资质文件url
            val certificateName: String, // 资质文件名称
            val certificateNumber: String, // string
            val certificateType: String // string
    )
}