package com.epc.easypurchase.bean

/**
 * Created by chenwuchao on 2018/11/24.
 */
data class PasswordBean(
        val cellphone:String,
        val newPassword:String,
        val type:Int,
        val verityCode:String
                      )