package com.jiazhuo.blockgamesquare.bean

/**
 * Created by Administrator on 2018/11/12.
 */
data class BaseBean<out T> (val code:Int,
                            val msg:String?,
                            val data: T?,
                            val success :Boolean)

