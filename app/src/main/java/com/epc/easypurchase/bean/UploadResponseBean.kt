package com.epc.easypurchase.bean

/**
 * Created by Administrator on 2018/11/27.
 */
data class UploadResponseBean(
        val upToken: String,
        val savingFileKey: String)