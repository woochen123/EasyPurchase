package com.jiazhuo.blockgamesquare.util

import android.util.Log
import android.widget.Toast
import com.epc.easypurchase.BuildConfig
import com.epc.easypurchase.AppApplication
import com.jiazhuo.blockgamesquare.constant.Constants

/**
 * Created by Administrator on 2018/11/9.
 */
fun Any.toast(msg: String?) {
    if (msg == null) return
    Constants.toast?.apply {
        setText(msg)
        show()
    } ?: run {
        Toast.makeText(AppApplication.mContext, null, Toast.LENGTH_SHORT).apply {
            setText(msg)
            Constants.toast = this
        }.show()
    }
}

fun Any.toast(stringId: Int) {
    toast(AppApplication.mContext.getString(stringId))
}

fun Any.debugToast(msg: String) {
    if (BuildConfig.DEBUG) toast(msg)
}

fun Any.debugToast(stringId: Int) {
    debugToast(AppApplication.mContext.getString(stringId))
}


fun Any.logee(msg: String){
    if (BuildConfig.DEBUG)
        Log.e(javaClass.simpleName, msg)
}
