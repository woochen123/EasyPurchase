package com.epc.easypurchase.util

import android.content.Context
import android.view.View

import com.epc.easypurchase.R
import com.jiazhuo.blockgamesquare.util.ResourceUtil
import com.woochen.baselibrary.dialog.AlertDialog

/**
 * 通用确弹窗
 * @author woochen
 * @time 2018/8/30 9:57
 */

object SimpleDialogUtil {

    interface SimpleClickListener {
        fun confirm()
    }


    /**
     * 普通确认取消弹框
     *
     * @param context
     * @param stringId
     * @param simpleClickListener
     */
    fun confirmAndCancel(context: Context, msgId: Int,cancelId: Int,confirmId: Int, simpleClickListener: SimpleClickListener?) {
        val builder = AlertDialog.Builder(context)
        builder.setContentView(R.layout.dialog_common)
                .setText(R.id.tv_message, ResourceUtil.getString(msgId))
                .setText(R.id.tv_cancel, ResourceUtil.getString(cancelId))
                .setText(R.id.tv_confirm, ResourceUtil.getString(confirmId))
                .setOnClickListener(R.id.tv_cancel, View.OnClickListener { builder.dialog.dismiss() })
                .setOnClickListener(R.id.tv_confirm, View.OnClickListener {
                    simpleClickListener?.confirm()
                    builder.dialog.dismiss()
                })
                .show()

    }


}
