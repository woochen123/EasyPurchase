package com.epc.easypurchase.util

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Administrator on 2018/9/25.
 */

object SPUtil {
    fun getSP(context: Context, name: String): SharedPreferences {
        return context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    fun putString(sp: SharedPreferences, key: String, value: String): Boolean {
        val editor = sp.edit()
        editor.putString(key, value)
        return editor.commit()
    }

    fun getString(sp: SharedPreferences, key: String): String {
        return sp.getString(key, "")
    }

    fun clearAll(sp: SharedPreferences) {
        val editor = sp.edit()
        editor.clear()
        editor.commit()
    }
}
