package com.epc.easypurchase.util

import java.io.File
import java.util.ArrayList

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody

/**
 * Created by Administrator on 2018/8/28.
 */

object RequestBodyUtil {
    /**
     * 将文件路径数组封装为[<]
     *
     * @param key       对应请求正文中name的值。目前服务器给出的接口中，所有图片文件使用<br></br>
     * 同一个name值，实际情况中有可能需要多个
     */

    fun filesToMultipartBodyParts(files: List<File>, key: String): List<MultipartBody.Part> {
        val parts = ArrayList<MultipartBody.Part>(files.size)
        for (file in files) {
            val requestBody = RequestBody.create(MediaType.parse("image/png"), file)
            val part = MultipartBody.Part.createFormData(key, file.name, requestBody)
            parts.add(part)
        }
        return parts
    }

    /**
     * 将参数封装成requestBody形式上传参数
     * @param param 参数
     * @return RequestBody
     */
    fun convertToRequestBody(param: String): RequestBody {
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), param)
    }

    /**
     * 将文件进行转换
     * @param param 为文件类型
     * @return
     */
    fun convertToRequestBodyMap(param: File): RequestBody {

        return RequestBody.create(MediaType.parse("multipart/form-data"), param)
    }
}
