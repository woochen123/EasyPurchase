package com.epc.easypurchase.util

import android.content.Context
import android.content.SharedPreferences

/**
 * 存储信息到sp中
 *
 * @author woochen123
 * @time 2017/3/21 15:52
 */
class SPUserUtil private constructor(context: Context) {

    init {
        sp = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
    }

    companion object {

        var PREFERENCE_NAME = "user_info"
        private var util: SPUserUtil? = null
        private lateinit var sp: SharedPreferences

        fun init(context: Context) {
            if (util == null) {
                util = SPUserUtil(context)
            }
        }

        fun putString(key: String, value: String): Boolean {
            val editor = sp.edit()
            editor.putString(key, value)
            return editor.commit()
        }

        @JvmOverloads
        fun getString(key: String, defaultValue: String = ""): String? {
            return sp.getString(key, defaultValue)
        }

        fun putInt(key: String, value: Int): Boolean {
            val editor = sp.edit()
            editor.putInt(key, value)
            return editor.commit()
        }

        @JvmOverloads
        fun getInt(key: String, defaultValue: Int = -1): Int {
            return sp.getInt(key, defaultValue)
        }

        fun putLong(context: Context, key: String, value: Long): Boolean {
            val editor = sp.edit()
            editor.putLong(key, value)
            return editor.commit()
        }


        @JvmOverloads
        fun getLong(key: String, defaultValue: Long = -1): Long {
            return sp.getLong(key, defaultValue)
        }

        fun putFloat(key: String, value: Float): Boolean {
            val editor = sp.edit()
            editor.putFloat(key, value)
            return editor.commit()
        }

        @JvmOverloads
        fun getFloat(key: String, defaultValue: Float = -1f): Float {
            return sp.getFloat(key, defaultValue)
        }


        fun putBoolean(key: String, value: Boolean): Boolean {
            val editor = sp.edit()
            editor.putBoolean(key, value)
            return editor.commit()
        }


        @JvmOverloads
        fun getBoolean(key: String, defaultValue: Boolean = false): Boolean {
            return sp.getBoolean(key, defaultValue)
        }

        fun clearAll() {
            val editor = sp.edit()
            editor.clear()
            editor.commit()
        }
    }
}