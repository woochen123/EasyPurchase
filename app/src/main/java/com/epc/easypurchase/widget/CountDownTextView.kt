package com.epc.easypurchase.widget


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import com.epc.easypurchase.helper.RxSchedulers
import com.epc.easypurchase.util.DefaultActivityLifecycleCallbacks
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import java.lang.ref.SoftReference
import java.util.concurrent.TimeUnit


/**
 * 倒计时控件
 *
 * @author woochen
 * @time 2018/8/24 9:12
 */

class CountDownTextView : TextView {

    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
//        mActivity.application.registerActivityLifecycleCallbacks(mLifecycle)
    }

    private val mCompositeDisposable = CompositeDisposable()

    private val mActivity: Activity = context as Activity

    private val mLifecycle = object : DefaultActivityLifecycleCallbacks() {

        override fun onActivityDestroyed(activity: Activity) {
            if (activity === mActivity) {
                mCompositeDisposable.clear()
            }
            mActivity.application.unregisterActivityLifecycleCallbacks(this)
        }
    }


    fun setCodeOnclickListener(codeOnclickListener: CodeOnclickListener) {
        super.setOnClickListener(codeOnclickListener)
    }

    abstract class CodeOnclickListener(countDownTextView: CountDownTextView) : View.OnClickListener {
        private val unableBackground: Drawable? = null//不可点击背景色
        private val enableBackground: Drawable? = null//可以点击背景色
        private val totalTime: Long = 60
        private val initialDelay: Long = 0

        private val mDownTextView: CountDownTextView

        private//                        mDownTextView.setBackgroundDrawable(unableBackground);
                //                    mDownTextView.setBackgroundDrawable(enableBackground);
        val observer: DisposableObserver<Long>
            get() = object : DisposableObserver<Long>() {
                @SuppressLint("SetTextI18n")
                override fun onNext(t: Long) = if (t == 0L) {
                    mDownTextView.text = "发送验证码"
                } else {
                    mDownTextView.text = t.toString() + "秒"
                }



                override fun onError(e: Throwable) {}

                override fun onComplete() {
                    mDownTextView.isEnabled = true
                }
            }

        abstract fun sendCode(): Boolean

        init {
            val softReference = SoftReference(countDownTextView)
            mDownTextView = softReference.get()!!
        }

        override fun onClick(v: View) {
            if (sendCode()) {
                val observer = Observable.interval(initialDelay, 1, TimeUnit.SECONDS)
                        .take(totalTime + 1)
                        .compose(RxSchedulers.io_main<Long,Long>())
                        .map { t ->
                            mDownTextView.isEnabled = false
                            totalTime - t - initialDelay
                        }.subscribeWith(observer) as DisposableObserver<*>
                mDownTextView.mCompositeDisposable.add(observer)
            }
        }
    }

    companion object {
        private val TAG = "CountDownTextView"
    }
}
