package com.epc.easypurchase

import android.app.Application
import android.content.Context
import com.epc.easypurchase.util.SPUserUtil
import com.jiazhuo.blockgamesquare.http.RetrofitUtil
import com.jiazhuo.blockgamesquare.http.api.UrlConstainer
import com.kingja.loadsir.callback.SuccessCallback
import com.kingja.loadsir.core.LoadSir
import com.tencent.bugly.crashreport.CrashReport
import com.woochen.baselibrary.base.callback.EmptyCallback
import com.woochen.baselibrary.base.callback.ErrorCallback
import com.woochen.baselibrary.base.callback.LoadingCallback
import com.woochen.baselibrary.base.callback.LoadingHasContentCallback

/**
 * Created by Administrator on 2018/11/12.
 */
class AppApplication : Application() {

    companion object {
         lateinit var  mContext : Context
    }

    override fun onCreate() {
        super.onCreate()
        mContext = this
        initSeverAddress()
        initBugly()
        initSp()
        initNet()
        initLoadSir()
    }

    private fun initSp() {
        SPUserUtil.init(mContext)
    }


    /**
     * 初始化bugly
     */
    private fun initBugly() {
        val strategy = CrashReport.UserStrategy(applicationContext)
        CrashReport.initCrashReport(applicationContext, "0ce6e1e2e0", false, strategy)
    }


    /**
     * 初始化页面加载视图
     */
    private fun initLoadSir() {
        LoadSir.beginBuilder()
                .addCallback(EmptyCallback())
                .addCallback(ErrorCallback())
                .addCallback(LoadingCallback())
                .addCallback(LoadingHasContentCallback())
                .setDefaultCallback(SuccessCallback::class.java)
                .commit()
    }

    /**
     * 服务器地址
     */
    private fun initSeverAddress() {
        if (BuildConfig.DEBUG){
            UrlConstainer.appBaseUrl = UrlConstainer.APP_TEST_URL
            UrlConstainer.webBaseUrl = UrlConstainer.WEB_TEST_URL
        }else{
            UrlConstainer.appBaseUrl = UrlConstainer.APP_RELEASE_URL
            UrlConstainer.webBaseUrl = UrlConstainer.WEB_RELEASE_URL

        }
    }

    /**
     * 初始化网络引擎
     */
    private fun initNet() {
        RetrofitUtil.getInstance().init()
    }
}