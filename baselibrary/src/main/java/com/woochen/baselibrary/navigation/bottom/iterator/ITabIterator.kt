package com.woochen.baselibrary.navigation.bottom.iterator


import com.woochen.baselibrary.navigation.bottom.TabItemView

/**
 * Created by admin on 2017/11/22.
 */

interface ITabIterator {
    /**
     * 返回下一个元素
     */
    operator fun next(): TabItemView<*>

    /**
     * 是否有下一个元素
     * @return
     */
    operator fun hasNext(): Boolean
}
