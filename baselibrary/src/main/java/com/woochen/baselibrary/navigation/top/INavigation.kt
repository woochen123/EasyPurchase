package com.woochen.baselibrary.navigation.top


/**
 * 创建人：chenwuchao
 * 创建时间：2017/5/17 16:39
 */
interface INavigation {

    // 绑定布局ID
    fun bindLayoutId(): Int

    // 给View设置参数
    fun applyView()
}
