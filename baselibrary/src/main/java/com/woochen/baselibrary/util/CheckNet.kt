package com.woochen.baselibrary.util

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

/**
 * 网络检测注解
 * @author chenwuchao
 * @time 2017/10/26 11:39
 * @desc
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(RetentionPolicy.RUNTIME)
annotation class CheckNet
