package com.woochen.baselibrary.base

import android.app.Application

/**
 * 基类application
 *
 * @author chenwuchao
 * @time 2017/9/1 16:35
 * @desc
 */
class BaseApplication : Application()
