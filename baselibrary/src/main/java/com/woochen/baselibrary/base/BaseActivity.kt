package com.woochen.baselibrary.base

import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.woochen.baselibrary.R


/**
 * Activity基类(不要修改)
 * Created by woochen123 on 2017/7/26.
 */

abstract class BaseActivity : AppCompatActivity() {
    protected lateinit var mContext: Context
    protected lateinit var mTopNavigation: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = this
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        setContentView(setContentView())
        initLoadSir()
        initData()
    }

    protected abstract fun initLoadSir()

    /**
     * 设置布局
     */
    protected abstract fun setContentView(): Int

    /**
     * 启动activity
     *
     * @param clazz
     */
    protected fun startActivity(clazz: Class<*>) {
        val intent = Intent(this, clazz)
        startActivity(intent)
    }

    /**
     * 初始化数据
     */
    protected abstract fun initData()


    override fun onBackPressed() {
        if (!isFinishing) {
            back()
        }
    }

    /**
     * 返回
     */
    protected open fun back() {
        finish()
    }

}
