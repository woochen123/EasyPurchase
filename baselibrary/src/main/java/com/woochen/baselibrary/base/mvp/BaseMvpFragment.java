package com.woochen.baselibrary.base.mvp;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kingja.loadsir.callback.Callback;
import com.kingja.loadsir.core.LoadService;
import com.kingja.loadsir.core.LoadSir;
import com.woochen.baselibrary.base.BaseFragment;
import com.woochen.baselibrary.base.callback.EmptyCallback;
import com.woochen.baselibrary.base.callback.ErrorCallback;
import com.woochen.baselibrary.base.callback.LoadingCallback;
import com.woochen.baselibrary.base.callback.LoadingHasContentCallback;
import com.woochen.baselibrary.base.mvp.proxy.IPresenterProxy;
import com.woochen.baselibrary.mvp.IBaseView;


/**
 *mvp fragment 基类
 *@author woochen123
 *@time 2018/1/19 15:45
 *@desc
 */

public abstract class BaseMvpFragment extends BaseFragment implements IBaseView {
    IPresenterProxy mPresenterProxy;
    private LoadService loadService;
    private static final int MESSAGE_RETRY_CODE = 1000;
    private boolean isRetrying;
    private Handler mRetryHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            isRetrying = false;
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mPresenterProxy = new FragmentPresenterProxyImpl<>(this);
        mPresenterProxy.bindPresenter();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    protected abstract void initView();

    @Override
    protected View initLoadSir(View rootView) {
        loadService = LoadSir.getDefault().register(rootView, new Callback.OnReloadListener() {
            @Override
            public void onReload(View v) {
                if (!isRetrying) {
                    isRetrying = true;
                    if(mRetryHandler != null){
                        mRetryHandler.sendEmptyMessageDelayed(MESSAGE_RETRY_CODE, 3000);
                        requestData();
                    }
                }else {
//                    ToastUtil.showToast(R.string.much_operate);
                }
            }
        });
//        showLoading(true);
        return loadService.getLoadLayout();
    }

    /**
     * 数据请求(重试)
     */
    protected void requestData() {

    }

    @Override
    public void onDestroy() {
        mPresenterProxy.unbindPresenter();
        super.onDestroy();
        if(mRetryHandler != null){
            mRetryHandler.removeCallbacksAndMessages(null);
            mRetryHandler =null;
        }
    }


    @Override
    public void showContent() {
        loadService.showSuccess();
    }

    @Override
    public void showEmpty() {
        loadService.showCallback(EmptyCallback.class);
    }

    @Override
    public void showError() {
        loadService.showCallback(ErrorCallback.class);
    }

    @Override
    public void showLoading(boolean showContent) {
        if(showContent){
            loadService.showCallback(LoadingHasContentCallback.class);
        }else {
            loadService.showCallback(LoadingCallback.class);
        }
    }

}
