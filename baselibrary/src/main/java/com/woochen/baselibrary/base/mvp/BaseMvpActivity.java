package com.woochen.baselibrary.base.mvp;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.View;

import com.kingja.loadsir.callback.Callback;
import com.kingja.loadsir.core.LoadService;
import com.kingja.loadsir.core.LoadSir;
import com.woochen.baselibrary.base.BaseActivity;
import com.woochen.baselibrary.base.callback.EmptyCallback;
import com.woochen.baselibrary.base.callback.ErrorCallback;
import com.woochen.baselibrary.base.callback.LoadingCallback;
import com.woochen.baselibrary.base.callback.LoadingHasContentCallback;
import com.woochen.baselibrary.base.mvp.proxy.IPresenterProxy;
import com.woochen.baselibrary.mvp.IBaseView;


/**
 * mvp activity基类
 *
 * @author woochen123
 * @time 2018/1/19 11:43
 * @desc
 */

public abstract class BaseMvpActivity extends BaseActivity implements IBaseView {
    protected IPresenterProxy mPresenterProxy;
    private LoadService loadService;
    private boolean isRetrying;
    private static final int MESSAGE_RETRY_CODE = 100;
    private Handler mRetryHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            isRetrying = false;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mPresenterProxy = new ActivityPresenterProxyImpl<>(this);
        mPresenterProxy.bindPresenter();
        super.onCreate(savedInstanceState);
    }




    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenterProxy.unbindPresenter();
        if(mRetryHandler != null){
            mRetryHandler.removeCallbacksAndMessages(null);
            mRetryHandler = null;
        }
    }

    @Override
    protected void initLoadSir() {
        loadService = LoadSir.getDefault().register(this, new Callback.OnReloadListener() {
            @Override
            public void onReload(View v) {
                if (!isRetrying) {
                    isRetrying = true;
                    if(mRetryHandler != null){
                        mRetryHandler.sendEmptyMessageDelayed(MESSAGE_RETRY_CODE, 3000);
                        onNetReload();
                    }
                } else {
//                    ToastUtil.showToast(R.string.much_operate);
                }
            }
        });
    }

    /**
     * 请求网络数据(错误重试执行)
     */
    protected void onNetReload() {

    }


    @Override
    public void showContent() {
        loadService.showSuccess();
    }

    @Override
    public void showEmpty() {
        loadService.showCallback(EmptyCallback.class);
    }

    @Override
    public void showError() {
        loadService.showCallback(ErrorCallback.class);
    }

    @Override
    public void showLoading(boolean showContent) {
        if (showContent) {
            loadService.showCallback(LoadingHasContentCallback.class);
        } else {
            loadService.showCallback(LoadingCallback.class);
        }
    }


}
