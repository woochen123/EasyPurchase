package com.woochen.baselibrary.base.mvp;

import io.reactivex.disposables.Disposable;

/**
 * Created by Administrator on 2018/8/23.
 */

public interface IPresenter {
    //添加指定的请求
    void addDisposable(Disposable disposable);
    //移除指定的请求
    void removeDisposable(Disposable disposable);
    //取消所有请求
    void removeAllDisposable();
}
