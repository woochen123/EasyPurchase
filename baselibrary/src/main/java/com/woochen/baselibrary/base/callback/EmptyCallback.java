package com.woochen.baselibrary.base.callback;

import android.content.Context;
import android.view.View;

import com.kingja.loadsir.callback.Callback;
import com.woochen.baselibrary.R;


/**
 *空页面回调
 *@author woochen
 *@time 2018/9/12 10:59
 */
public class EmptyCallback extends Callback {

    @Override
    protected int onCreateView() {
        return R.layout.layout_empty;
    }


    @Override
    protected void onViewCreate(Context context, View view) {
        view.setClickable(false);

    }
}
