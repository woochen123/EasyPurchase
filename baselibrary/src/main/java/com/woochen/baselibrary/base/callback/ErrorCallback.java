package com.woochen.baselibrary.base.callback;


import android.content.Context;
import android.view.View;

import com.kingja.loadsir.callback.Callback;
import com.woochen.baselibrary.R;


/**
 *错误页面回调
 *@author woochen
 *@time 2018/9/12 10:59
 */

public class ErrorCallback extends Callback {
    @Override
    protected int onCreateView() {
        return R.layout.layout_error;
    }

    @Override
    protected void onViewCreate(Context context, final View view) {
        /*view.setClickable(false);
        view.findViewById(R.id.tv_retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.performClick();
            }
        });*/
    }
}
