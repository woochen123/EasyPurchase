package com.woochen.baselibrary.base.mvp.proxy;

/**
 *persneter代理类
 *@author woochen123
 *@time 2018/1/19 11:25
 *@desc
 */

public interface IPresenterProxy {
    void bindPresenter();
    void unbindPresenter();
}
