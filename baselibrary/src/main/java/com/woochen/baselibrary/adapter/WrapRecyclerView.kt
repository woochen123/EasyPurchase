package com.woochen.baselibrary.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.View

/**
 * 项目名称：joke
 * 类描述：可以添加头部和底部的recycleview
 * 创建人：woochen123
 * 创建时间：2017/10/1 11:36
 * 必须要先调用setAdapter方法后才能添加和移除头部（否则）
 */
class WrapRecyclerView : RecyclerView {
    private var mWrapAdapter: WrapRecyclerAdapter? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {}

    override fun setAdapter(adapter: RecyclerView.Adapter<*>) {
        //替换系统的adapter
        mWrapAdapter = WrapRecyclerAdapter(adapter)
        super.setAdapter(mWrapAdapter)
    }

    /**
     * 添加头部View
     * @param view
     */
    fun addHeadView(view: View) {
        if (mWrapAdapter == null) {
            throw IllegalArgumentException("please invoke setAdapter() first!")
        }
        mWrapAdapter!!.addHeaderView(view)
    }

    /**
     * 移除头部View
     * @param view
     */
    fun removeHeaderView(view: View) {
        if (mWrapAdapter != null) {
            mWrapAdapter!!.removeHeaderView(view)
        }
    }

    /**
     * 添加尾部View
     * @param view
     */
    fun addFooterView(view: View) {
        if (mWrapAdapter == null) {
            throw IllegalArgumentException("please invoke setAdapter() first!")
        }
        mWrapAdapter!!.addFooterView(view)
    }

    /**
     * 移除底部View
     * @param view
     */
    fun removeFooterView(view: View) {
        if (mWrapAdapter != null) {
            mWrapAdapter!!.removeFooterView(view)
        }
    }

}
