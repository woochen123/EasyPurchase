package com.woochen.baselibrary.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * 项目名称：joke
 * 类描述：万能RecyclerView适配器
 * 创建人：woochen123
 * 创建时间：2017/5/23 18:32
 */
public abstract class CommonRecyclerAdapter<T> extends RecyclerView.Adapter<CommonRecyclerAdapter.ViewHolder> {
    protected Context mContext;
    private LayoutInflater mInflater;
    //数据怎么办？利用泛型
    protected List<T> mDatas;
    // 布局怎么办？直接从构造里面传递
    private int mLayoutId;
    // 多布局支持
    private MultiTypeSupport mMultiTypeSupport;

    public CommonRecyclerAdapter(Context mContext, List<T> datas, int mLayoutId) {
        this.mContext = mContext;
        this.mDatas = datas;
        this.mLayoutId = mLayoutId;
        this.mInflater = LayoutInflater.from(mContext);
    }

    /**
     * 多布局支持
     */
    public CommonRecyclerAdapter(Context context, List<T> data, MultiTypeSupport<T> multiTypeSupport) {
        this(context, data, -1);
        this.mMultiTypeSupport = multiTypeSupport;
    }

    /**
     * 根据当前位置获取不同的viewType
     */
    @Override
    public int getItemViewType(int position) {
        // 多布局支持
        if (mMultiTypeSupport != null) {
            return mMultiTypeSupport.getLayoutId(mDatas.get(position), position);
        }
        return super.getItemViewType(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // 多布局支持
        if (mMultiTypeSupport != null) {
            mLayoutId = viewType;
        }

        View itemView = mInflater.inflate(mLayoutId, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // 设置点击和长按事件
        if (mItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemClickListener.onItemClick(position);
                }
            });
        }
        if (mLongClickListener != null) {
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return mLongClickListener.onLongClick(position);
                }
            });
        }
        if(mHoverListener!=null){
            holder.itemView.setOnHoverListener(new View.OnHoverListener() {
                @Override
                public boolean onHover(View view, MotionEvent motionEvent) {
                    return mHoverListener.onHover(view,motionEvent);
                }
            });
        }

        // 绑定怎么办？回传出去
        convert(holder, mDatas.get(position));
    }

    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    /**
     * 利用抽象方法回传出去，每个不一样的Adapter去设置
     * @param item 当前的数据
     */
    public abstract void convert(ViewHolder holder, T item);

    public static class ViewHolder extends RecyclerView.ViewHolder{
         // 用来存放子View减少findViewById的次数
         private SparseArray<View> mViews;

        public ViewHolder(View itemView) {
             super(itemView);
             mViews = new SparseArray<>();
         }

         /**
          * 设置TextView文本
          */
         public ViewHolder setText(int viewId, CharSequence text) {
             TextView tv = getView(viewId);
             tv.setText(text);
             return this;
         }

         /**
          * 通过id获取view
          */
         public <T extends View> T getView(int viewId) {
             // 先从缓存中找
             View view = mViews.get(viewId);
             if (view == null) {
                 // 直接从ItemView中找
                 view = itemView.findViewById(viewId);
                 mViews.put(viewId, view);
             }
             return (T) view;
         }

         /**
          * 设置View的Visibility
          */
         public ViewHolder setViewVisibility(int viewId, int visibility) {
             getView(viewId).setVisibility(visibility);
             return this;
         }

         /**
          * 设置ImageView的资源
          */
         public ViewHolder setImageResource(int viewId, int resourceId) {
             ImageView imageView = getView(viewId);
             imageView.setImageResource(resourceId);
             return this;
         }

         /**
          * 设置条目点击事件
          */
         public void setOnIntemClickListener(View.OnClickListener listener) {
             itemView.setOnClickListener(listener);
         }

         /**
          * 设置条目长按事件
          */
         public void setOnIntemLongClickListener(View.OnLongClickListener listener) {
             itemView.setOnLongClickListener(listener);
         }

        /**
         * 设置条目悬浮事件
         * @param hoverListener
         */
        public void setmHoverListener(View.OnHoverListener hoverListener){
            itemView.setOnHoverListener(hoverListener);
        }

    }

    /*
     * 设置条目点击和长按事件
     */
    public OnItemClickListener mItemClickListener;
    public OnLongClickListener mLongClickListener;
    public View.OnHoverListener mHoverListener;

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.mItemClickListener = itemClickListener;
    }

    public void setOnLongClickListener(OnLongClickListener longClickListener) {
        this.mLongClickListener = longClickListener;
    }
    public void setmHoverListener(View.OnHoverListener hoverListener){
        this.mHoverListener = hoverListener;
    }

}
