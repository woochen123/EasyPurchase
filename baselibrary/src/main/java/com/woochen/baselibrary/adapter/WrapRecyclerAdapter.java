package com.woochen.baselibrary.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * 项目名称：joke
 * 类描述：可以添加头部和底部的RecyclerView.Adapter
 * 创建人：woochen123
 * 创建时间：2017/10/1 10:52
 */
public class WrapRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    //原来的adapter
    private RecyclerView.Adapter mRealAdapter;
    //头部
    private ArrayList<View> mHeaderViews;
    //底部
    private ArrayList<View> mFooterViews;

    public WrapRecyclerAdapter(RecyclerView.Adapter realAdapter) {
        this.mRealAdapter = realAdapter;
        mHeaderViews = new ArrayList<>();
        mFooterViews = new ArrayList<>();
        //当真正的适配器数据改变时，通知包裹后的适配器进行数据的更新
        mRealAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        int numHeader = mHeaderViews.size();
        if (position < numHeader) {
            return new ExtraViewViewHolder(mHeaderViews.get(position));
        }
        int realAdapterPosition = position - numHeader;
        int realAdapterCount = 0;
        if (mRealAdapter != null) {
            realAdapterCount = mRealAdapter.getItemCount();
            if (realAdapterPosition < realAdapterCount) {
                return mRealAdapter.onCreateViewHolder(parent, mRealAdapter.getItemViewType(realAdapterPosition));
            }
        }
        return new ExtraViewViewHolder(mFooterViews.get(realAdapterPosition - realAdapterCount));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int numHeader = mHeaderViews.size();
        if (position < numHeader) {
            return;
        }
        int realAdapterPosition = position - numHeader;
        int realAdapterCount = 0;
        if (mRealAdapter != null) {
            realAdapterCount = mRealAdapter.getItemCount();
            if (realAdapterPosition < realAdapterCount) {
                mRealAdapter.onBindViewHolder(holder, realAdapterPosition);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mHeaderViews.size() + mRealAdapter.getItemCount() + mFooterViews.size();
    }

    static class ExtraViewViewHolder extends RecyclerView.ViewHolder {

        public ExtraViewViewHolder(View itemView) {
            super(itemView);
        }
    }


    /**
     * 添加底部View
     * @param view
     */
    public void addFooterView(View view) {
        if (!mFooterViews.contains(view)) {
            mFooterViews.add(view);
            notifyDataSetChanged();
        }
    }

    /**
     * 添加头部View
     * @param view
     */
    public void addHeaderView(View view) {
        if (!mHeaderViews.contains(view)) {
            mHeaderViews.add(view);
            notifyDataSetChanged();
        }
    }

    /**
     * 移除底部View
     * @param view
     */
    public void removeFooterView(View view) {
        if (!mFooterViews.contains(view)) {
            mFooterViews.remove(view);
            notifyDataSetChanged();
        }
    }

    /**
     * 移除头部View
     * @param view
     */
    public void removeHeaderView(View view) {
        if (!mHeaderViews.contains(view)) {
            mHeaderViews.remove(view);
            notifyDataSetChanged();
        }
    }


}
